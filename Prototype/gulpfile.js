// Alphabetical Order
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var concat = require('gulp-concat');
var fileinclude = require('gulp-file-include');
//var fs = require('fs');
var gulp = require("gulp");
var imagemin = require('gulp-imagemin');
var inject = require('gulp-inject');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var rename = require("gulp-rename");
var replace = require('gulp-replace');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var svgmin = require('gulp-svgmin');
var uglify = require("gulp-uglify");
var util = require("gulp-util");
var vinylNamed = require('vinyl-named'); // allows use of [name] in gulp-webpack output
var webpack = require("webpack");
var webpackConfig = require("./webpack.config");
var webpackStream = require('webpack-stream'); // Webpack enabled for use mid-stream


/* INJECT (Includes, JS, CSS) ================================================= */
gulp.task('inject', function() {
  return gulp.src('./assets/layouts/src/**/*.html')
    // Includes
    .pipe(fileinclude({
      prefix: '@@',
      indent: true,
      basepath: './assets/includes/**'
    }))
    .pipe(replace(/[\u200B-\u200D\uFEFF]/g, "")) // Strip BOM being added (gulp-file-replace issue)
    .pipe(gulp.dest('./assets'))
    // CSS + JS Inject
    .pipe(inject(
      gulp.src(['./assets/client/css/main.css', './assets/client/js/dist/client.bundle.js'], { read: false }), { relative: true }))
    .pipe(gulp.dest('./assets'))
    //.pipe(notify({ message: "Inject Task Done."}) )
    .pipe(browserSync.stream());
});


/* CSS (Compile, Sourcemaps, Prefixing) ================================================= */
gulp.task('css', function () {
  return gulp.src('./assets/client/css/**/*.scss')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(plumber())
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(concat('main.css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./assets/client/css'))
    //.pipe(notify({ message: "CSS Task Done."}) )
    .pipe(browserSync.stream());
});


/* MINIFY CSS (Minify) ================================================= */
gulp.task('minify-css', () => {
  return gulp.src('./assets/client/css/main.css')
    .pipe(cleanCSS({compatibility: '*'}))
    .pipe(concat('main.min.css'))
    //.pipe(notify({ message: "Minify CSS Done."}) )
    .pipe(gulp.dest('./assets/client/css'));
});


/* JS ================================================================ */
gulp.task("js", function () {
  return gulp.src('./assets/client/js/main.js')
      .pipe(vinylNamed())
      .pipe(webpackStream(webpackConfig))
      .pipe(gulp.dest('./assets/client/js/dist'))
      //.pipe(notify({ message: "JS Task Done."}) )
});

// gulp.task("min:js", function () {
//     webpackConfig.plugins = [
//         new webpack.optimize.UglifyJsPlugin({
//             compress: { warnings: false },
//             minimize: true
//         })
//     ];
//     webpackConfig.devtool = "#source-map";
//     return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
//         .pipe(vinylNamed())
//         .pipe(webpackStream(webpackConfig))
//         .pipe(gulp.dest("."));
// });


/* IMAGES (Optimize Images) ================================================================ */
gulp.task('images', function() {
  return gulp.src('./assets/client/images/src/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./assets/client/images/dist'))
    //.pipe(notify({ message: "Image Task Done."}) )
});


/* SVGO ================================================================ */
gulp.task('svgo', function () {
  return gulp.src('./assets/client/images/src/**/*.svg')
      .pipe(svgmin({
          js2svg: {
              pretty: true
          },
          plugins: [
              {
                  removeTitle: true
              }
          ]
      }))
      .pipe(gulp.dest('./assets/client/images/dist'))
      //.pipe(notify({ message: "SVGO Task Done."}) )
});


/* BROWSER-SYNC ================================================= */
gulp.task('browser-sync', function () {
  browserSync.init({
      server: {
          baseDir: "./assets/"
      }
  });
});


/* WATCH ================================================= */
gulp.task('watch', function() {
  // HTML
  gulp.watch('./assets/**/*.html', ['inject']);
  // CSS
  gulp.watch('./assets/client/css/**/*.scss', ['css']);
  // JS
  gulp.watch(['./assets/client/js/*.js', './assets/client/js/src/*.js'], ['js']);
});


/* SERVE (Prototype) ================================================= */
gulp.task('serve', function () {
  gulp.start('svgo', 'images', 'css', 'minify-css', 'inject', 'css', 'js', 'browser-sync', 'watch');
});