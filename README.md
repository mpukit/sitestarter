# Prototype
### HTML/CSS/JS Prototype using **[Bootstrap 4](http://getbootstrap.com/)** and **[Gulp](https://gulpjs.org/)** as a task-runner.
*Requires **[Gulp](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)** and **[Node](https://nodejs.org/en/download/)**.

---

Navigate to the root directory of the Prototype:
```js
cd Prototype
```

Run 'npm install' to install the required dependencies:
```js
npm install
```

Then run 'gulp serve' in the root of the directory to start watching files and launch the project in your browser:
```js
gulp serve
```

---

